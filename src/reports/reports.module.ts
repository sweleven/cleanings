import { HttpModule, Module } from '@nestjs/common';
import { ReportsService } from './services/reports.service';
import { ReportsController } from './controllers/reports.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Cleaning,
  CleaningSchema,
} from 'src/cleanings/schemas/cleaning.schema';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: Cleaning.name, schema: CleaningSchema },
    ]),
  ],
  controllers: [ReportsController],
  providers: [ReportsService],
})
export class ReportsModule {}
