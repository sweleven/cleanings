import { HttpService, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cleaning } from 'src/cleanings/schemas/cleaning.schema';
import { CreateReportDto } from '../dto/create-report.dto';
import { Model } from 'mongoose';
import { from } from 'rxjs';

@Injectable()
export class ReportsService {
  constructor(
    @InjectModel(Cleaning.name) private readonly cleaningModel: Model<Cleaning>,
    private readonly http: HttpService,
  ) {}

  create(createReportDto: CreateReportDto) {
    Logger.log(JSON.stringify(createReportDto));

    const { users, workspaces } = createReportDto;
    let { start_date, end_date } = createReportDto;

    if (!end_date) {
      end_date = Date.now();
    } else {
      end_date = end_date.getTime();
    }

    start_date = start_date.getTime();

    return from(
      this.cleaningModel
        .find({
          user: {
            $in: users,
          },
          workspace: {
            $in: workspaces,
          },
          datetime: {
            $gte: start_date,
            $lte: end_date,
          },
        })
        .sort('start_datetime')
        .exec(),
    );
  }
}
