import { IsArray, IsDate, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreateReportDto {
  @IsArray()
  @ApiProperty()
  users: string[];

  @IsArray()
  @ApiProperty()
  workspaces: string[];

  @ApiProperty()
  @Type(() => Date)
  @IsDate()
  start_date: any;

  @IsOptional()
  @ApiProperty()
  @Type(() => Date)
  @IsDate()
  end_date: any;
}
