import { Module } from '@nestjs/common';
import { HistoryService } from './services/history.service';
import { HistoryController } from './controllers/history.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Cleaning,
  CleaningSchema,
} from 'src/cleanings/schemas/cleaning.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Cleaning.name, schema: CleaningSchema },
    ]),
  ],
  controllers: [HistoryController],
  providers: [HistoryService],
})
export class HistoryModule {}
