import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { from, Observable } from 'rxjs';
import { Model } from 'mongoose';
import { Cleaning } from 'src/cleanings/schemas/cleaning.schema';

@Injectable()
export class HistoryService {
  constructor(
    @InjectModel(Cleaning.name) private readonly cleaningModel: Model<Cleaning>,
  ) {}

  findAll(headers: Headers): Observable<Cleaning[]> {
    const userInfo = this._getUserinfo(headers);

    return this.findHistoryForUser(userInfo.email);
  }

  findOneByEmail(email: string, headers: Headers): Observable<Cleaning[]> {
    const userInfo = this._getUserinfo(headers);

    // If user is not internal nor admin and is requesting history of another user
    if (
      userInfo !== 'internal' &&
      !userInfo.realm_access.roles.includes('admin') &&
      userInfo.email !== email
    ) {
      return null;
    }

    return this.findHistoryForUser(email);
  }

  findOneByWorkspace(
    workspace_id: string,
    headers: Headers,
  ): Observable<Cleaning[]> {
    const userInfo = this._getUserinfo(headers);

    // If user is not internal nor admin
    if (
      userInfo !== 'internal' &&
      !userInfo.realm_access.roles.includes('admin')
    ) {
      return null;
    }

    return this.findHistoryForWorkspace(workspace_id);
  }

  public findHistoryForUser(email: string): Observable<Cleaning[]> {
    return from(
      this.cleaningModel.find({ user: email }).sort('datetime').exec(),
    );
  }

  public findHistoryForWorkspace(workspace_id: string): Observable<Cleaning[]> {
    return from(
      this.cleaningModel
        .find({ workspace: workspace_id })
        .sort('end_datetime')
        .exec(),
    );
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }
}
