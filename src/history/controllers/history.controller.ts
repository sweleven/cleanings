import { Controller, Get, Param, Headers } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { Cleaning } from 'src/cleanings/schemas/cleaning.schema';
import { HistoryService } from '../services/history.service';

@Controller('history')
export class HistoryController {
  constructor(private readonly historyService: HistoryService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'The list of all Cleanings',
    type: [Cleaning],
  })
  findAll(@Headers() headers: Headers): Observable<Cleaning[]> {
    return this.historyService.findAll(headers);
  }

  @Get('/user/:user_email')
  @ApiResponse({
    status: 200,
    description: 'The list of all Cleanings for a user',
    type: [Cleaning],
  })
  findOneByEmail(
    @Param('user_email') email: string,
    @Headers() headers: Headers,
  ): Observable<Cleaning[]> {
    return this.historyService.findOneByEmail(email, headers);
  }

  @Get('/workspace/:workspace_id')
  @ApiResponse({
    status: 200,
    description: 'The list of all bookings for a workspace',
    type: [Cleaning],
  })
  findOneByWorkspace(
    @Param('workspace_id') workspace_id: string,
    @Headers() headers: Headers,
  ): Observable<Cleaning[]> {
    return this.historyService.findOneByWorkspace(workspace_id, headers);
  }
}
