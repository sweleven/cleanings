import { HttpService, Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { combineLatest, from, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CreateCleaningDto } from '../dto/create-cleaning.dto';
import { SearchCleaningDto } from '../dto/search-cleaning.dto';
import { Cleaning } from '../schemas/cleaning.schema';

@Injectable()
export class CleaningsService {
  /**
   * Creates an instance of CleaningsService.
   * @param {Model<Cleaning>} cleaningModel
   * @memberof CleaningsService
   */
  constructor(
    @InjectModel(Cleaning.name) private readonly cleaningModel: Model<Cleaning>,
    @Inject('INVENTORY_SERVICE') private inventoryClient: ClientProxy,
    private readonly http: HttpService,
  ) {}

  create(
    createCleaningDto: CreateCleaningDto,
    headers,
  ): Observable<Cleaning> | Observable<Cleaning[]> {
    Logger.log('This action adds a new cleaning');

    // Get user from jwt token
    const userinfo = this._getUserinfo(headers);

    // Please god, forgive me
    return this.http
      .get(`http://inventory:3002/rfid/${createCleaningDto.rfid}`)
      .pipe(
        map((response) => response.data),
        switchMap((data) => {
          if (data.workspaces) {
            let cleaning: Cleaning;
            let cleanings: Observable<Cleaning>[] = [];

            // Cleaning a room
            data.workspaces.forEach((workspace) => {
              cleaning = new this.cleaningModel({
                workspace: workspace,
                datetime: Date.now(),
                user: userinfo.username,
              });
              cleanings = cleanings.concat(from(cleaning.save()));
            });

            return combineLatest(cleanings).pipe(
              switchMap((_cleanings: Cleaning[]) => {
                return from(_cleanings);
              }),
              switchMap((_cleaning: Cleaning) => {
                return this._set_workspace_clean(_cleaning.workspace).pipe(
                  map(() => _cleaning),
                );
              }),
            );
          } else {
            // Cleaning a workspace
            const cleaning = new this.cleaningModel({
              workspace: data,
              datetime: Date.now(),
              user: userinfo.username,
            });
            return from(cleaning.save()).pipe(
              switchMap((_cleaning: Cleaning) => {
                return this._set_workspace_clean(_cleaning.workspace).pipe(
                  map(() => _cleaning),
                );
              }),
            );
          }
        }),
      );
  }

  /**
   *
   *
   * @param {SearchCleaningDto} searchQuery
   * @return {*}  {Observable<Cleaning[]>}
   * @memberof CleaningsService
   */
  findAll(searchQuery: SearchCleaningDto, headers): Observable<Cleaning[]> {
    Logger.log(`This action returns all cleanings`);

    // Get user from jwt token
    const userinfo = this._getUserinfo(headers);
    let filter = {};

    if (
      userinfo !== 'internal' &&
      (!userinfo.realm_access || !userinfo.realm_access.roles.includes('admin'))
    ) {
      filter = { user: userinfo.username };
    }

    return from(this.cleaningModel.find(filter).sort('date').exec());
  }

  /**
   *
   *
   * @param {string} id
   * @return {*}  {Observable<Cleaning>}
   * @memberof CleaningsService
   */
  findOne(id: string, headers): Observable<Cleaning> {
    Logger.log(`This action returns a #${id} cleaning`);

    // Get user from jwt token
    const userinfo = this._getUserinfo(headers);
    let filter = {};

    if (
      userinfo !== 'internal' &&
      (!userinfo.realm_access || !userinfo.realm_access.roles.includes('admin'))
    ) {
      filter = { id: id, user: userinfo.username };
    } else {
      filter = { id: id };
    }

    return from(this.cleaningModel.findOne(filter).exec());
  }

  private _getUserinfo(headers: Headers): any {
    const userinfo = headers['x-userinfo'];
    if (userinfo !== null && userinfo !== undefined) {
      return JSON.parse(Buffer.from(userinfo, 'base64').toString('binary'));
    } else {
      return 'internal';
    }
  }

  private _set_workspace_clean(workspace_id: string): Observable<void> {
    return this.inventoryClient.emit('setWorkspaceCleanState', {
      workspace: workspace_id,
      state: true,
    });
  }
}
