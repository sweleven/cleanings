import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MongooseModule } from '@nestjs/mongoose';
import { CleaningsController } from './controllers/cleanings.controller';
import { Cleaning, CleaningSchema } from './schemas/cleaning.schema';
import { CleaningsService } from './services/cleanings.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: Cleaning.name, schema: CleaningSchema },
    ]),
    ClientsModule.registerAsync([
      {
        name: 'INVENTORY_SERVICE',
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [
              `amqp://${configService.get('RABBITMQ_USER')}:${configService.get(
                'RABBITMQ_PASSWORD',
              )}@${configService.get('RABBITMQ_HOST')}:${configService.get(
                'RABBITMQ_PORT',
              )}`,
            ],
            queue: configService.get('INVENTORY_QUEUE_NAME'),
            queueOptions: {
              durable: true,
            },
          },
        }),
        inject: [ConfigService],
      },
    ]),
  ],
  controllers: [CleaningsController],
  providers: [CleaningsService],
})
export class CleaningsModule {}
