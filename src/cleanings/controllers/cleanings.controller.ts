import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Query,
  Headers,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { CreateCleaningDto } from '../dto/create-cleaning.dto';
import { SearchCleaningDto } from '../dto/search-cleaning.dto';
import { FindOneParams } from '../params/find-one.params';
import { Cleaning } from '../schemas/cleaning.schema';
import { CleaningsService } from '../services/cleanings.service';

@Controller('cleanings')
export class CleaningsController {
  constructor(private readonly cleaningsService: CleaningsService) {}

  @Post()
  create(
    @Body() createCleaningDto: CreateCleaningDto,
    @Headers() headers: Headers,
  ) {
    return this.cleaningsService.create(createCleaningDto, headers);
  }

  @Get()
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'All existing records',
    type: [Cleaning],
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid id format provided',
  })
  @ApiResponse({
    status: 404,
    description: 'No record found',
  })
  findAll(
    @Query() searchQuery: SearchCleaningDto,
    @Headers() headers: Headers,
  ): Observable<Cleaning[]> {
    return this.cleaningsService.findAll(searchQuery, headers);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: String,
  })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Cleaning,
  })
  @ApiResponse({
    status: 404,
    description: 'No records found',
  })
  findOne(
    @Param() { id }: FindOneParams,
    @Headers() headers: Headers,
  ): Observable<Cleaning> {
    return this.cleaningsService.findOne(id, headers);
  }
}
