import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { IsString } from 'class-validator';

@Schema({ autoIndex: true, toJSON: { virtuals: true }, id: false })
export class Cleaning extends Document {
  @ApiProperty()
  @Prop()
  workspace: string;

  @ApiProperty()
  @Prop()
  datetime: number;

  @ApiProperty()
  @IsString()
  @Prop()
  user: string;
}

export const CleaningSchema = SchemaFactory.createForClass(Cleaning);
