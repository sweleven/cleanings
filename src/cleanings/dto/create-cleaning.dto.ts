import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateCleaningDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  rfid: string;
}
